# Overview

Simple Demo Script to showcase the [Dynatrace Push Event Pipe](https://bitbucket.org/dynatrace/dynatrace-push-event/src/master/).

This demo will send two [Dynatrace information events](https://www.dynatrace.com/support/help/dynatrace-api/environment-api/events/post-event/) that are used to enable continuous delivery tools, such as BitBucket, to provide additional details for Dynatrace. 

There are several Dynatrace information event types:

* CUSTOM_ANNOTATION
* CUSTOM_CONFIGURATION
* CUSTOM_DEPLOYMENT
* CUSTOM_INFO
* MARKED_FOR_TERMINATION

This demo pipeline will send a `CUSTOM_DEPLOYMENT` and a `CUSTOM_ANNOTATION` informational event.

# Setup

This assumes the [Dynatrace Orders sample application](https://github.com/dt-orders/overview) is running and that the require pipeline variables with the [Dynatrace](http://dynatrace.com/) URL and API token are configured per the [Dynatrace Push Event Pipe](https://bitbucket.org/dynatrace/dynatrace-push-event/src/master/) instructions.

# Maintainer

[Rob Jahn](https://www.linkedin.com/in/robjahn/) -- Email me @ rob.jahn@dynatrace.com with questions or more details.